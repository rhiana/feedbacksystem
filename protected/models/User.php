<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property string $IntID
 * @property string $uname
 * @property string $email
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uname, email', 'required'),
			array('uname, email', 'length', 'max'=>45),
                        array('IntID, issue_id', 'numerical', 'integerOnly'=>true),
                        array('uname, upassword', 'length','max'=>45),
                        array('uname, ctime', 'required'),
                    // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('IntID, uname, email, issue_id, upassword, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'IntID' => 'Int',
			'uname' => 'Uname',
			'email' => 'Email',
                        'issue_id' => 'issue_id',
                        'upassword' => 'upassword',
                        'ctime'=> 'ctime'
		);
	}

        
        public function behaviors()
            {
                return array(
                    'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                    'createAttribute' => 'create_time',
                    'updateAttribute' => 'update_time',
                    'setUpdateOnCreate' => true,
                    ),
                );
            }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('IntID',$this->IntID,true);
		$criteria->compare('uname',$this->uname,true);
		$criteria->compare('email',$this->email,true);
                $criteria->compare('issue_id',$this->issue_id,true);
                $criteria->compare('upassword',$this->upassword,true);
            //    $criteria->compare('Type',$this->ctype_id,true);
                $criteria->compare('ctime',$this->ctime,true);                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

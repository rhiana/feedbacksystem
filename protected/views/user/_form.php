<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'uname'); ?>
		<?php echo $form->textField($model,'uname',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'uname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'issue_id'); ?>
		<?php echo $form->dropDownList($model,'issue_id', $issue_list, array("prompt" => "select Issue Type")); ?>
		<?php echo $form->error($model,'issue_id'); ?>
	</div>
        
                
	<div class="row">
		<?php echo $form->labelEx($model,'upassword'); ?>
		<?php echo $form->textField($model,'upassword',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'upassword'); ?>
	</div>
        
        <div class="row">
                <?php echo $form->labelEx($model,'ctime'); ?>
                <?php echo $form->textField($model,'ctime'); ?>
                <?php echo $form->error($model,'ctime'); ?>
         </div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
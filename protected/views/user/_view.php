<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Number')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->IntID), array('view', 'id'=>$data->IntID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user name')); ?>:</b>
	<?php echo CHtml::encode($data->uname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issue_id')); ?>:</b>
	<?php echo CHtml::encode($data->issue_id); ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('upassword')); ?>:</b>
	<?php echo CHtml::encode($data->upassword); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />
</div>